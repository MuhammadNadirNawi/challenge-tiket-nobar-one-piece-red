# Challenge Tiket Nobar One Piece Red

Challenge disini adalah memodifikasi file berikut:

- [`scripts/filterByEyeColor.js`](./filterByEyeColor.js)
**!!! Perhatian cukup melakukan perubahan di file ini.**

## `filterByEyeColor`

Disini kamu akan membuat sebuah fungsi yang berguna untuk menyaring data dari file json,
yang mana dari data tersebut ketika sudah disaring/filter,
akan menyisakan daftar data yang eycolornya bernilai blue.

berikut input dan ouput yang dihasilkan:

### Input
inputnya berasal dari - [`scripts/generated.json`](./generated.json)

```json
[
  {
    "_id": "63297845736bafbbcdfb75e8",
    "index": 0,
    "guid": "ddfe0243-1c6d-4493-adf9-da6c2f2ffa9c",
    "isActive": true,
    "balance": "$3,351.73",
    "picture": "http://placehold.it/32x32",
    "age": 26,
    "eyeColor": "blue",
    "name": "Alison Shannon",
    "gender": "female",
    "company": "GLUKGLUK",
    "email": "alisonshannon@glukgluk.com",
    "phone": "+1 (963) 548-2189",
    "address": "431 Caton Avenue, Why, New York, 7913",
    "about": "Velit cillum officia est exercitation commodo culpa esse in elit magna laborum ad. Sunt id do exercitation quis laborum nostrud proident ad aliquip. Non ex fugiat incididunt sint consequat magna laboris aliqua.\r\n",
    "registered": "2017-06-02T12:32:56 -08:00"
  },
  {
    "_id": "632978452c521841cfa31747",
    "index": 1,
    "guid": "27734621-4c65-491a-8aa2-77bcb69c9bd7",
    "isActive": true,
    "balance": "$2,104.07",
    "picture": "http://placehold.it/32x32",
    "age": 34,
    "eyeColor": "brown",
    "name": "Obrien Lambert",
    "gender": "male",
    "company": "ENJOLA",
    "email": "obrienlambert@enjola.com",
    "phone": "+1 (854) 508-2946",
    "address": "876 Denton Place, Carlton, Kansas, 7062",
    "about": "Cillum nisi occaecat tempor proident sit ullamco. Nostrud magna est irure ullamco ex deserunt culpa adipisicing voluptate non. Anim sint deserunt aute est cillum cupidatat id velit nulla incididunt irure ullamco ullamco tempor. Consequat cillum eu laboris officia ullamco. Proident adipisicing ipsum qui pariatur ipsum eiusmod eu cupidatat eu sint est sunt.\r\n",
    "registered": "2014-09-30T02:51:49 -08:00"
  },
  {
    "_id": "632978454dd1745c3990d164",
    "index": 2,
    "guid": "61ac03e8-25c1-4295-99c9-8b5a02ca7719",
    "isActive": false,
    "balance": "$3,733.85",
    "picture": "http://placehold.it/32x32",
    "age": 24,
    "eyeColor": "blue",
    "name": "Blanchard Stephens",
    "gender": "male",
    "company": "ZYTREK",
    "email": "blanchardstephens@zytrek.com",
    "phone": "+1 (990) 564-2047",
    "address": "530 Carroll Street, Soham, Georgia, 3096",
    "about": "Fugiat minim consequat irure duis nisi dolore sunt reprehenderit adipisicing in ut. Enim in id qui ut dolor ullamco dolore. Occaecat laborum fugiat laboris aute laborum reprehenderit tempor voluptate. Ut ut sint occaecat amet sit nisi exercitation amet adipisicing officia.\r\n",
    "registered": "2020-04-22T05:58:20 -08:00"
  },
  {
    "_id": "63297845dde98519c3b4a78b",
    "index": 3,
    "guid": "13ce63de-74ba-4efe-aada-458102f72862",
    "isActive": true,
    "balance": "$2,118.87",
    "picture": "http://placehold.it/32x32",
    "age": 39,
    "eyeColor": "brown",
    "name": "Amelia Baxter",
    "gender": "female",
    "company": "KOZGENE",
    "email": "ameliabaxter@kozgene.com",
    "phone": "+1 (994) 558-2317",
    "address": "247 Story Court, Rose, Rhode Island, 5399",
    "about": "Aliquip sit ad ex duis elit esse commodo reprehenderit aliqua id aliqua ad exercitation ipsum. Occaecat voluptate irure ex pariatur laboris esse deserunt nisi adipisicing officia occaecat incididunt. Consequat ea pariatur eiusmod ut non aute amet sit voluptate reprehenderit deserunt. Nisi deserunt commodo non ea consequat duis aute ea quis. Dolor elit minim ut quis eiusmod non occaecat esse fugiat eu laboris labore. Minim aliquip sunt ex eu dolore.\r\n",
    "registered": "2021-09-04T05:32:05 -08:00"
  },
  {
    "_id": "6329784576b8ba3ea749094f",
    "index": 4,
    "guid": "0e3327cc-50a3-41ea-8821-a3e8d880e312",
    "isActive": true,
    "balance": "$3,943.17",
    "picture": "http://placehold.it/32x32",
    "age": 27,
    "eyeColor": "brown",
    "name": "Sharpe Hart",
    "gender": "male",
    "company": "PAPRIKUT",
    "email": "sharpehart@paprikut.com",
    "phone": "+1 (848) 567-3145",
    "address": "583 Dahill Road, Eggertsville, New Mexico, 3917",
    "about": "Quis elit duis eiusmod quis esse ex aliqua voluptate consequat eu. Officia do consequat reprehenderit et ipsum consectetur commodo eu cillum magna tempor minim nisi. Officia excepteur do dolor aute reprehenderit ullamco ut enim ex nisi esse aute voluptate. Proident dolore est eiusmod velit excepteur tempor laborum magna esse in ea adipisicing veniam reprehenderit.\r\n",
    "registered": "2020-03-05T12:56:49 -08:00"
  },
  {
    "_id": "632978455dde62d6ab7f061b",
    "index": 5,
    "guid": "1db21266-c014-4d56-b13c-2f010a87feb6",
    "isActive": true,
    "balance": "$3,740.31",
    "picture": "http://placehold.it/32x32",
    "age": 36,
    "eyeColor": "blue",
    "name": "Penelope Frost",
    "gender": "female",
    "company": "GREEKER",
    "email": "penelopefrost@greeker.com",
    "phone": "+1 (859) 550-2383",
    "address": "868 Portland Avenue, Sardis, Oregon, 6945",
    "about": "Quis laboris Lorem sunt sunt sint qui do cillum anim non adipisicing consectetur dolor duis. Dolore eiusmod et qui ex. Elit commodo aliqua aute Lorem pariatur consequat elit labore qui pariatur nulla. Irure tempor consectetur mollit nulla qui duis voluptate consequat occaecat. Adipisicing mollit pariatur dolor minim deserunt do velit.\r\n",
    "registered": "2020-08-05T06:51:05 -08:00"
  },
  {
    "_id": "63297845e06be0a17857127f",
    "index": 6,
    "guid": "91c25638-a8d1-4b11-92d8-ba60f0ebd69d",
    "isActive": false,
    "balance": "$3,623.80",
    "picture": "http://placehold.it/32x32",
    "age": 37,
    "eyeColor": "blue",
    "name": "Kellie Jackson",
    "gender": "female",
    "company": "OCTOCORE",
    "email": "kelliejackson@octocore.com",
    "phone": "+1 (976) 489-2883",
    "address": "622 Schenck Avenue, Gordon, Arizona, 5464",
    "about": "Aute sit occaecat exercitation officia veniam ut est amet excepteur. Lorem enim enim occaecat nostrud eiusmod enim culpa ipsum dolore qui do enim laborum non. Amet do ut id anim reprehenderit occaecat. Ut cupidatat esse elit fugiat ea dolore aliqua dolore id irure enim. Aliqua sunt esse occaecat ipsum consequat minim qui voluptate occaecat tempor. Fugiat reprehenderit consectetur tempor dolore labore quis enim anim.\r\n",
    "registered": "2022-04-28T01:35:05 -08:00"
  }
]
```

### Output

Ketika data `generated.json` dimasukkan ke dalam fungsi `filterByEyeColor`,
maka hasil tiap item-nya memiliki atribut `eyecolor` yang bernilai `blue`.

```json
[
  {
    "_id": "63297845736bafbbcdfb75e8",
    "index": 0,
    "guid": "ddfe0243-1c6d-4493-adf9-da6c2f2ffa9c",
    "isActive": true,
    "balance": "$3,351.73",
    "picture": "http://placehold.it/32x32",
    "age": 26,
    "eyeColor": "blue",
    "name": "Alison Shannon",
    "gender": "female",
    "company": "GLUKGLUK",
    "email": "alisonshannon@glukgluk.com",
    "phone": "+1 (963) 548-2189",
    "address": "431 Caton Avenue, Why, New York, 7913",
    "about": "Velit cillum officia est exercitation commodo culpa esse in elit magna laborum ad. Sunt id do exercitation quis laborum nostrud proident ad aliquip. Non ex fugiat incididunt sint consequat magna laboris aliqua.\r\n",
    "registered": "2017-06-02T12:32:56 -08:00"
  },
  {
    "_id": "632978454dd1745c3990d164",
    "index": 2,
    "guid": "61ac03e8-25c1-4295-99c9-8b5a02ca7719",
    "isActive": false,
    "balance": "$3,733.85",
    "picture": "http://placehold.it/32x32",
    "age": 24,
    "eyeColor": "blue",
    "name": "Blanchard Stephens",
    "gender": "male",
    "company": "ZYTREK",
    "email": "blanchardstephens@zytrek.com",
    "phone": "+1 (990) 564-2047",
    "address": "530 Carroll Street, Soham, Georgia, 3096",
    "about": "Fugiat minim consequat irure duis nisi dolore sunt reprehenderit adipisicing in ut. Enim in id qui ut dolor ullamco dolore. Occaecat laborum fugiat laboris aute laborum reprehenderit tempor voluptate. Ut ut sint occaecat amet sit nisi exercitation amet adipisicing officia.\r\n",
    "registered": "2020-04-22T05:58:20 -08:00"
  },
  {
    "_id": "632978455dde62d6ab7f061b",
    "index": 5,
    "guid": "1db21266-c014-4d56-b13c-2f010a87feb6",
    "isActive": true,
    "balance": "$3,740.31",
    "picture": "http://placehold.it/32x32",
    "age": 36,
    "eyeColor": "blue",
    "name": "Penelope Frost",
    "gender": "female",
    "company": "GREEKER",
    "email": "penelopefrost@greeker.com",
    "phone": "+1 (859) 550-2383",
    "address": "868 Portland Avenue, Sardis, Oregon, 6945",
    "about": "Quis laboris Lorem sunt sunt sint qui do cillum anim non adipisicing consectetur dolor duis. Dolore eiusmod et qui ex. Elit commodo aliqua aute Lorem pariatur consequat elit labore qui pariatur nulla. Irure tempor consectetur mollit nulla qui duis voluptate consequat occaecat. Adipisicing mollit pariatur dolor minim deserunt do velit.\r\n",
    "registered": "2020-08-05T06:51:05 -08:00"
  },
  {
    "_id": "63297845e06be0a17857127f",
    "index": 6,
    "guid": "91c25638-a8d1-4b11-92d8-ba60f0ebd69d",
    "isActive": false,
    "balance": "$3,623.80",
    "picture": "http://placehold.it/32x32",
    "age": 37,
    "eyeColor": "blue",
    "name": "Kellie Jackson",
    "gender": "female",
    "company": "OCTOCORE",
    "email": "kelliejackson@octocore.com",
    "phone": "+1 (976) 489-2883",
    "address": "622 Schenck Avenue, Gordon, Arizona, 5464",
    "about": "Aute sit occaecat exercitation officia veniam ut est amet excepteur. Lorem enim enim occaecat nostrud eiusmod enim culpa ipsum dolore qui do enim laborum non. Amet do ut id anim reprehenderit occaecat. Ut cupidatat esse elit fugiat ea dolore aliqua dolore id irure enim. Aliqua sunt esse occaecat ipsum consequat minim qui voluptate occaecat tempor. Fugiat reprehenderit consectetur tempor dolore labore quis enim anim.\r\n",
    "registered": "2022-04-28T01:35:05 -08:00"
  }
]
```

```javascript
 console.log("Good Luck 🤩🤩🤩");
```










